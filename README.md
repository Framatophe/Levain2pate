# Voici Levain2pate

Aide au calcul des proportions d'une pâte à pain au levain **naturel**.

## Mode d'emploi

Remplir les champs correspondants :

   * Taux d'humidité du levain
   * Poids de levain disponible
   * Proportion de levain (% du poids de farine)
   * Taux d'humidité souhaité de la pâte à pain
   * Proportion de sel en fonction du poids de  l'eau de coulage (entre 2 et 3,5%)

Levain2pate vous calcule alors les poids de farine, d'eau de coulage et de sel à ajouter au levain pour préparer la pâte à pain.

-----

Levain2pate est placé sous Licence [MIT](https://opensource.org/licenses/MIT)
Copyright 2015 Violaine & Christophe Masutti

Levain2pate a été élaboré à l'aide d'une simple feuille de calcul issue de LibreOffice (.ods) et transformé en application avec [Appizy](http://www.appizy.com/), un convertisseur sous [licence MIT](https://opensource.org/licenses/MIT). Le reste est une affaire de css.



